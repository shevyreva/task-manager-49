package ru.t1.shevyreva.tm.jms.operation;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class OperationEvent {

    @Nullable
    private OperationType type;

    @Nullable
    private Object entity;

    @Nullable
    private String table;

    @NotNull
    private final Long timestamp = System.currentTimeMillis();

    public OperationEvent(@NotNull final OperationType type, @NotNull final Object entity) {
        this.type = type;
        this.entity = entity;
    }

}
