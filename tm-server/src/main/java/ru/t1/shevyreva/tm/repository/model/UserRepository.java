package ru.t1.shevyreva.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.model.IUserRepository;
import ru.t1.shevyreva.tm.model.User;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepository extends AbstractModelRepository<User> implements IUserRepository {


    public UserRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        entityManager.remove(findAll());
    }

    @Override
    public @Nullable List<User> findAll() {
        return entityManager.createQuery("SELECT e FROM User e", User.class).getResultList();
    }

    @Override
    public @Nullable User findOneById(@NotNull String id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public void removeOneById(@NotNull String id) {
        entityManager.remove(findOneById(id));
    }

    @Override
    public int getSize() {
        return entityManager.createQuery("SELECT COUNT(e) FROM User e", Integer.class)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Nullable
    public User findByLogin(@NotNull final String login) {
        return entityManager.createQuery("SELECT e FROM User e WHERE e.login =:login", User.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("login", login)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    public User findByEmail(@NotNull final String email) {
        return entityManager.createQuery("SELECT e FROM User e WHERE e.email = :email", User.class)
                .setParameter("email", email)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeOne(@NotNull final User user) {
        entityManager.remove(user);
    }

}
