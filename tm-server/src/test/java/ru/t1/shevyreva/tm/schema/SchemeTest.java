package ru.t1.shevyreva.tm.schema;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.shevyreva.tm.marker.MigrationCategory;

@Category(MigrationCategory.class)
public class SchemeTest extends AbstractSchemeTest {

    @Test
    public void test() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

}
