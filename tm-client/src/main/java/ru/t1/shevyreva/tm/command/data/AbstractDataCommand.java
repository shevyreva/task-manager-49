package ru.t1.shevyreva.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.endpoint.IDomainEndpoint;
import ru.t1.shevyreva.tm.command.AbstractCommand;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public static final String FILE_XML = "./data.xml";

    @NotNull
    public static final String FILE_JSON = "./data.json";

    @NotNull
    public static final String FILE_YAML = "./data.yaml";

    @NotNull
    public final String CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    public final String CONTEXT_FACTORY_JAXB = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    protected final String MEDIA_TYPE = "eclipselink.media-type";

    @NotNull
    protected final String APPLICATION_TYPE_JSON = "application/json";

    public AbstractDataCommand() {

    }

    @Nullable
    public IDomainEndpoint getDomainEndpoint() {
        if (getServiceLocator() == null) return null;
        return getServiceLocator().getDomainEndpoint();
    }

}
