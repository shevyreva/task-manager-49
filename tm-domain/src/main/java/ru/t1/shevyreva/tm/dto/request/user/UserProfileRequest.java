package ru.t1.shevyreva.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class UserProfileRequest extends AbstractUserRequest {

    public UserProfileRequest(@Nullable String token) {
        super(token);
    }

}
