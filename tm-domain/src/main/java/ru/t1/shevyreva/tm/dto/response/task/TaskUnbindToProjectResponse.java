package ru.t1.shevyreva.tm.dto.response.task;

import lombok.Getter;
import lombok.Setter;
import ru.t1.shevyreva.tm.dto.response.AbstractResponse;

@Setter
@Getter
public class TaskUnbindToProjectResponse extends AbstractResponse {
}
